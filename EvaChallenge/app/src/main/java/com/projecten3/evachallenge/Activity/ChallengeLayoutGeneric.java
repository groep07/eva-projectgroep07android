package com.projecten3.evachallenge.Activity;

import android.app.Activity;
import android.os.Bundle;

import com.projecten3.evachallenge.R;

/**
 * Created by tomas on 19/10/15.
 */
public class ChallengeLayoutGeneric extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_layout_generic);
    }

}
