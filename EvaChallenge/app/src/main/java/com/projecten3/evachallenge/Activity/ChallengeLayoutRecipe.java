package com.projecten3.evachallenge.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ExpandableListView;

import com.projecten3.evachallenge.Domain.ExpandableListAdapter;
import com.projecten3.evachallenge.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/**
 * Created by tomas on 19/10/15.
 */
public class ChallengeLayoutRecipe extends Activity {

    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    private GestureDetector gestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_layout_recipe);

        gestureDetector = new GestureDetector(new SwipeGestureDetector());

        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.expandableListView);

        // preparing list data
        prepareListData();

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(gestureDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_left_in, R.anim.activity_right_out);
    }

    private void onSwipeRight() {
        this.finish();
        overridePendingTransition(R.anim.activity_left_in, R.anim.activity_right_out);
    }

    /* Preparing the list data  */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add(getResources().getString(R.string.headerVegetarian));
        listDataHeader.add(getResources().getString(R.string.headerGlutenfree));
        listDataHeader.add(getResources().getString(R.string.headerSugarfree));
        listDataHeader.add(getResources().getString(R.string.headerChildFriendly));

        // Adding child data
        List<String> vegetarian = new ArrayList<String>();
        vegetarian.add("Hier komt een recept");


        List<String> glutenfree = new ArrayList<String>();
        glutenfree.add("Hier komt een recept");


        List<String> sugarfree = new ArrayList<String>();
        sugarfree.add("Hier komt een recept");

        List<String> childFriendly = new ArrayList<String>();
        childFriendly.add("Hier komt een recept");

        listDataChild.put(listDataHeader.get(0), vegetarian); // Header, Child data
        listDataChild.put(listDataHeader.get(1), glutenfree);
        listDataChild.put(listDataHeader.get(2), sugarfree);
        listDataChild.put(listDataHeader.get(3), childFriendly);
    }

    private class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_MIN_DISTANCE = 50;
        private static final int SWIPE_MAX_OFF_PATH = 200;
        private static final int SWIPE_THRESHOLD_VELOCITY = 50;

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float velocityX, float velocityY) {
            try {
                float yDistance = Math.abs(e1.getY() - e2.getY());
                float xDistance = Math.abs(e1.getX() - e2.getX());

                if(yDistance > SWIPE_MAX_OFF_PATH)
                    return false;

                //Swipe down
                if(velocityX > SWIPE_THRESHOLD_VELOCITY && xDistance > SWIPE_MIN_DISTANCE) {
                    if(e1.getX() < e2.getX())
                        ChallengeLayoutRecipe.this.onSwipeRight();
                }

            } catch(Exception e) {
                Log.e("EvaChallenge", "Error on gesture");
            }
            return false;
        }
    }
}
