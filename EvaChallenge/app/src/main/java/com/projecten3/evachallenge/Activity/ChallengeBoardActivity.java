package com.projecten3.evachallenge.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.view.View.OnClickListener;

import com.projecten3.evachallenge.R;


public class ChallengeBoardActivity extends Activity {

    private ImageButton btnProfile;
    private Button btnChallenge1,btnChallenge2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_board);

        final ScrollView scrollView = (ScrollView) findViewById(R.id.mainScrollView);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });


        btnProfile = (ImageButton) findViewById(R.id.btn_profile);
        btnProfile.setOnClickListener(onClickListener);

        btnChallenge1 = (Button) findViewById(R.id.btn_challenge1);
        btnChallenge1.setOnClickListener(onClickListener);
        btnChallenge2 = (Button) findViewById(R.id.btn_challenge2);
        btnChallenge2.setOnClickListener(onClickListener);
    }

    private OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent;
            switch(view.getId()) {
                case R.id.btn_profile:
                    intent = new Intent(getApplicationContext(), ProfileActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.activity_bottom_in,R.anim.activity_bottom_out);
                    break;
                case R.id.btn_challenge1:
                    intent = new Intent(getApplicationContext(),ChallengeLayoutRecipe.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.activity_right_in,R.anim.activity_left_out);
                    break;
                case R.id.btn_challenge2:
                    intent = new Intent(getApplicationContext(),ChallengeLayoutRestaurant.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.activity_right_in, R.anim.activity_left_out);
            }
        }
    };
}
