package com.projecten3.evachallenge.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ExpandableListView;

import com.projecten3.evachallenge.Domain.ExpandableListAdapter;
import com.projecten3.evachallenge.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ChallengeLayoutRestaurant extends Activity {

    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    private GestureDetector gestureDetector;
    private String[] cityList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_layout_restaurant);

        gestureDetector = new GestureDetector(new SwipeGestureDetector());

        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.expandableListViewRestaurant);

        //get the cities from the xml file
        cityList = getResources().getStringArray(R.array.cityList);

        // preparing list data
        prepareListData();

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(gestureDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_left_in, R.anim.activity_right_out);
    }

    private void onSwipeRight() {
        this.finish();
        overridePendingTransition(R.anim.activity_left_in, R.anim.activity_right_out);
    }

    /* Preparing the list data  */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add(getResources().getString(R.string.headerCities));
        listDataHeader.add(getResources().getString(R.string.headerRestaurant));
        listDataHeader.add(getResources().getString(R.string.headerSnacks));
        listDataHeader.add(getResources().getString(R.string.headerGastronomic));

        // Adding child data
        List<String> cities = new ArrayList<String>();
        Collections.addAll(cities, cityList);

        List<String> restaurant = new ArrayList<String>();
        restaurant.add("Hier komt een zaak");

        List<String> snacks = new ArrayList<String>();
        snacks.add("Hier komt een zaak");

        List<String> gastronomic = new ArrayList<String>();
        gastronomic.add("Hier komt een zaak");

        listDataChild.put(listDataHeader.get(0), cities); // Header, Child data
        listDataChild.put(listDataHeader.get(1), restaurant);
        listDataChild.put(listDataHeader.get(2), snacks);
        listDataChild.put(listDataHeader.get(3), gastronomic);
    }

    private class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_MIN_DISTANCE = 50;
        private static final int SWIPE_MAX_OFF_PATH = 200;
        private static final int SWIPE_THRESHOLD_VELOCITY = 50;

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float velocityX, float velocityY) {
            try {
                float yDistance = Math.abs(e1.getY() - e2.getY());
                float xDistance = Math.abs(e1.getX() - e2.getX());

                if(yDistance > SWIPE_MAX_OFF_PATH)
                    return false;

                //Swipe down
                if(velocityX > SWIPE_THRESHOLD_VELOCITY && xDistance > SWIPE_MIN_DISTANCE) {
                    if(e1.getX() < e2.getX())
                        ChallengeLayoutRestaurant.this.onSwipeRight();
                }

            } catch(Exception e) {
                Log.e("EvaChallenge", "Error on gesture");
            }
            return false;
        }
    }
}
