package com.projecten3.evachallenge.Activity;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.projecten3.evachallenge.R;

import java.util.ArrayList;
import java.util.List;


public class ProfileActivity extends Activity {

    private EditText etxVoornaam, etxWachtwoord, etxNaam;
    private TextView txv1,txv2, txv3, txv4, txv5;
    private SeekBar seekBar;
    private List<TextView> txvList;

    private GestureDetector gestureDetector;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        gestureDetector = new GestureDetector(new SwipeGestureDetector());


        etxNaam = (EditText) findViewById(R.id.voornaam);
        etxVoornaam = (EditText) findViewById(R.id.naam);
        etxWachtwoord = (EditText) findViewById(R.id.wachtwoord);
        seekBar = (SeekBar) findViewById(R.id.seekBar);

        txvList = new ArrayList<TextView>();

        txv1 = (TextView) findViewById(R.id.txv1);
        txv2 = (TextView) findViewById(R.id.txv2);
        txv3 = (TextView) findViewById(R.id.txv3);
        txv4 = (TextView) findViewById(R.id.txv4);
        txv5 = (TextView) findViewById(R.id.txv5);

        txvList.add(txv1);
        txvList.add(txv2);
        txvList.add(txv3);
        txvList.add(txv4);
        txvList.add(txv5);


        txv1.setTextColor(getResources().getColor(R.color.evaGreen));

        seekBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    int progress = 0;


                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progresValue, boolean fromUser) {
                        if (fromUser)
                            progress = progresValue;

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // Do something here,
                        //if you want to do anything at the start of
                        // touching the seekbar
                        for (int i = 0; i < txvList.size(); i++) {
                            txvList.get(i).setTextColor((getResources().getColor(R.color.evaBlack)));

                        }

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // Display the value in textview
                        txvList.get(progress).setTextColor((getResources().getColor(R.color.evaGreen)));

                    }
                });

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(gestureDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_back_bottom_in, R.anim.activity_back_bottom_out);
    }


    private void onSwipeDown() {
        this.finish();
        overridePendingTransition(R.anim.activity_back_bottom_in, R.anim.activity_back_bottom_out);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_MIN_DISTANCE = 50;
        private static final int SWIPE_MAX_OFF_PATH = 200;
        private static final int SWIPE_THRESHOLD_VELOCITY = 50;

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float velocityX, float velocityY) {
            try {
                float yDistance = Math.abs(e1.getY() - e2.getY());
                float xDistance = Math.abs(e1.getX() - e2.getX());

                if(xDistance > SWIPE_MAX_OFF_PATH)
                    return false;

                //Swipe down
                if(velocityY > SWIPE_THRESHOLD_VELOCITY && yDistance > SWIPE_MIN_DISTANCE) {
                    if(e1.getY() < e2.getY())
                        ProfileActivity.this.onSwipeDown();
                }

            } catch(Exception e) {
                Log.e("EvaChallenge", "Error on gesture");
            }
            return false;
        }
    }
}
