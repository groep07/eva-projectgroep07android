package com.projecten3.evachallenge.Fonts;

/**
 * Created by tomas on 19/10/15.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class Georgia_Body_TextView extends TextView {
    public Georgia_Body_TextView(Context context) {
        super(context);
        setFont();
    }
    public Georgia_Body_TextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }
    public Georgia_Body_TextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/Georgia.ttf");
        setTypeface(font, Typeface.NORMAL);
    }
}